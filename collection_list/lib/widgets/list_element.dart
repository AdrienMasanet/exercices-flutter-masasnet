import 'package:collection_list/models/car.dart';
import 'package:flutter/material.dart';

class ListElement extends StatelessWidget {
  const ListElement({Key? key, required this.element}) : super(key: key);

  final Car element;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(const Radius.circular(5.0)),
          image: element.imageUrl != null
              ? DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(element.imageUrl ?? ''))
              : null,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.4),
              spreadRadius: 0,
              blurRadius: 4,
              offset: Offset(1.0, 1.0), // changes position of shadow
            ),
          ],
        ),
        padding: const EdgeInsets.all(5.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
          Container(
            padding: EdgeInsets.all(5.0),
            decoration: const BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
            child: FittedBox(
              child: Text(
                element.description ?? 'pas de description',
                textAlign: TextAlign.center,
                style: const TextStyle(color: Colors.white, shadows: [
                  Shadow(
                    offset: Offset(1.0, 1.0),
                    blurRadius: 3.0,
                    color: Color.fromARGB(255, 0, 0, 0),
                  )
                ]),
              ),
            ),
          ),
        ]));
  }
}
