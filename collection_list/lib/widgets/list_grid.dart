import 'package:collection_list/models/car.dart';
import 'package:collection_list/views/car_detail.dart';
import 'package:collection_list/widgets/list_element.dart';
import 'package:flutter/material.dart';

class ListGrid extends StatelessWidget {
  final items = [
    Car("https://static1.hotcarsimages.com/wordpress/wp-content/uploads/2018/04/464BB34A00000578-5077819-image-a-74_1510587698252-e1524565722533.jpg?q=50&fit=crop&w=961&dpr=1.5",
        "une voiture moche"),
    Car("https://www.tuningblog.eu/wp-content/uploads/2019/06/Alien-style-Tuning-Chrysler-PT-Cruiser.jpg",
        "une autre voiture moche"),
    Car("https://static.cargurus.com/images/site/2010/02/25/04/17/pic-2143131838963504008-1600x1200.jpeg",
        "elle est pas mal celle-là aussi"),
    Car("http://4.bp.blogspot.com/-UUmvVU66FAs/Tg8mqs8QXII/AAAAAAAAF5k/nRlpcvWxOIA/s1600/ugly%2Bcars%2Bpictures-3.jpg",
        "beurk"),
    Car("https://img-9gag-fun.9cache.com/photo/aGR6WR5_460s.jpg",
        "c'est quoi ça ???"),
    Car("https://i.imgur.com/93FGXRQ.jpg", "Le mec qui a dû inventer ça..."),
    Car("https://assets3.cbsnewsstatic.com/hub/i/r/2011/04/21/116ba4f5-a643-11e2-a3f0-029118418759/thumbnail/1200x630/91189dc9f0d729e388e50d9437f6a4bc/Corbin_Sparrow.jpg",
        "MDRRRR"),
    Car("https://cdn.now.howstuffworks.com/media-content/ef0b4879-ed45-4d7b-be0c-bd186fd52aa1-1920-1080.jpg",
        "Oskour mdr"),
    Car("https://i.ytimg.com/vi/JC5gRmnUfRE/maxresdefault.jpg", "Yyyyyey"),
    Car("https://live.staticflickr.com/3278/2903070368_ef6c69fe2b_z.jpg",
        "Voiture-flasque"),
    Car("https://gnnhd.tv/assets/images/englishnews/resized/570x360/rthrt.webp",
        "Beaufland"),
    Car("https://i.pinimg.com/originals/56/0e/f6/560ef69c8735061747b91adefbe2660a.jpg",
        "J'en peux plus"),
    Car("https://i.imgur.com/wJdVQHQ.jpg", "J'abandonne"),
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: EdgeInsets.all(15.0),
      itemCount: items.length,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 15,
        mainAxisSpacing: 15,
      ),
      itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => CarDetail(carReviewing: items[index],)));
          },
          child: ListElement(element: items[index])),
    );
  }
}
