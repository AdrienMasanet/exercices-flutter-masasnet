import 'package:collection_list/models/car.dart';
import 'package:flutter/material.dart';

class CarDetail extends StatelessWidget {
  const CarDetail({Key? key, required this.carReviewing}) : super(key: key);

  final Car carReviewing;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Voiture nulle"),
        ),
        body: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Column(
            children: [
              Image(image: NetworkImage(carReviewing.imageUrl ?? '')),
              const Spacer(),
              Text(
                carReviewing.description ?? 'pas de description',
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Spacer(),
            ],
          ),
        ));
  }
}
