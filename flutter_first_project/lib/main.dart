import 'package:flutter/material.dart';

void main() {
  runApp(const MonApp());
}

class MonApp extends StatelessWidget {
  const MonApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "ma première app",
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("L'appli flutter"),
        ),
        body: Container(
            padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text(
                        "Pierre",
                        style: TextStyle(
                            fontSize: 25.0, color: Colors.deepOrangeAccent),
                      ),
                      Text("Paul"),
                      Text("Jacques"),
                    ],
                  ),
                  const Text("Texte 1"),
                  const Text("Texte 2"),
                  const Text("Texte 3"),
                  const Text("Texte 4"),
                  const Text("Texte 5"),
                  Container(
                    padding: const EdgeInsets.all(20.0),
                    child: Card(
                      child: Container(
                        padding: const EdgeInsets.all(25.0),
                        child: Column(
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(bottom: 20.0),
                              child: Text(
                                "Le joli chaton :",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 30.0, color: Colors.white),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 80.0),
                              child: Image.asset("images/ivo.jpeg"),
                            ),
                            Image.network(
                                "https://media-exp1.licdn.com/dms/image/C4E03AQFMEEGheOFt6w/profile-displayphoto-shrink_200_200/0/1642202922478?e=1659571200&v=beta&t=qsAkbj-9d2saq1TPbGDOIOmNz-uQiKacZ4HeuOXCmvc"),
                            const MaCard(
                                title: "Macard", image: "images/ivo.jpeg")
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )),
      ),
    );
  }
}

class MaCard extends StatelessWidget {
  const MaCard({required this.title, required this.image});

  final String title;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 25.0),
      child: Card(
        child: Container(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              title,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 30.0, color: Colors.deepPurple),
            ),
            Image.asset(image)
          ],
        )),
      ),
    );
  }
}
