import 'package:flutter/material.dart';
import 'package:formulaire/model/user.dart';
import 'package:formulaire/util/db_helper.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  TextEditingController identifierController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String dropDownValue = "Choisir un rôle";
  DbHelper dbHelper = DbHelper();
  late User user = User(0, '','','','','');

  Future testUser(String login, String password) async {
    await dbHelper.openDb();
    user = await dbHelper.getUser(login, password);

    setState(() {
      user = user;
    });
  }

  showAlert(BuildContext context, String message) {
    AlertDialog dialog = AlertDialog(
      title: const Text("Alert"),
      content: Text(message),
      actions: [
        ElevatedButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: const Text("Ok !")),
        ElevatedButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: const Text("Annuler")),
      ],
    );

    Future futureValue = showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("S'authentifier"),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 300,
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.blueAccent,
            border: Border.all(color: Colors.indigo, width: 1),
            borderRadius: BorderRadius.circular(18.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            children: [
              TextField(
                decoration: const InputDecoration(
                    hintText: "Identifiant",
                    hintStyle: TextStyle(color: Colors.white70)),
                controller: identifierController,
                cursorColor: Colors.white,
              ),
              TextField(
                decoration: const InputDecoration(
                    hintText: "Mot de passe",
                    hintStyle: TextStyle(color: Colors.white70)),
                controller: passwordController,
                obscureText: true,
                cursorColor: Colors.white,
              ),
              DropdownButton<String>(
                  hint: Text(
                    dropDownValue,
                    style: const TextStyle(color: Colors.white70, fontSize: 25),
                  ),
                  style: const TextStyle(color: Colors.white70, fontSize: 25),
                  dropdownColor: Colors.blueGrey,
                  items: <String>[
                    "Choisir un rôle",
                    "Admin",
                    "Agent",
                    "Utilisateur",
                  ].map<DropdownMenuItem<String>>((e) {
                    return DropdownMenuItem<String>(
                        value: e,
                        child: Text(
                          e,
                          style: const TextStyle(color: Colors.white70),
                        ));
                  }).toList(),
                  onChanged: (newValue) => {
                        setState(() {
                          dropDownValue = newValue!;
                        })
                      }),
              const Spacer(),
              Row(
                children: [
                  const Spacer(),
                  ElevatedButton(
                      onPressed: () {
                        testUser(identifierController.text, passwordController.text);
                        
                        if(user.id>0) {
                          Navigator.pushNamed(context, "/accueil");
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Heureux de vous revoir, ${user.firstname} !"))
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: const Text("Utilisateur non trouvé"))
                          );
                        }
                      },
                      child: const Text("Connexion")),
                  const Spacer(),
                  ElevatedButton(
                      onPressed: () => ScaffoldMessenger.of(context)
                          .showSnackBar(const SnackBar(
                              content: Text("Vous pouvez vous inscrire"))),
                      child: const Text("Inscription")),
                  const Spacer(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
