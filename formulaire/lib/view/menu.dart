import 'package:flutter/material.dart';
import 'package:formulaire/view/home.dart';

class Menu extends StatelessWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _menu = {
      "Accueil": [const Home(), const Icon(Icons.home_filled)]
    };

    List<Widget> lSide = [];
    lSide.add(Container(
      height: 30,
      child: const DrawerHeader(
        child: Text("Menu"),
      ),
    ));

    _menu.forEach((key, value) => lSide.add(ListTile(
          leading: value[1],
          title: Text(key),
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => value[0]));
          },
        )));

    return ListView(
      children: lSide,
    );
  }
}
