class Group {
  int id;
  String label;
  int order;

  Group(this.id, this.label, this.order);

  Map<String, dynamic> toMap() {
    return {
      'id': (id==0) ? null : id,
      'label': label,
      'order': order
    };
  }
}