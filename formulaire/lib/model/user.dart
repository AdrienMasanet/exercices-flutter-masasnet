class User {
  int id;
  String lastname;
  String firstname;
  String login;
  String password;
  String mail;

  User(this.id, this.lastname, this.firstname, this.login, this.password,
      this.mail);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'lastname': lastname,
      'firstname': firstname,
      'login': login,
      'password': password,
      'mail': mail
    };
  }
}