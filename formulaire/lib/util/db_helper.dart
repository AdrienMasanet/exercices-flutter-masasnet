import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:formulaire/model/user.dart';
import 'package:formulaire/model/group.dart';

class DbHelper {
  final int _version = 1;
  static var _db;

  static final DbHelper _dbHelper = DbHelper._internal();

  DbHelper._internal();

  factory DbHelper(){
    return _dbHelper;
  }

  /*
  Création de la base de données SQLite
   */
  Future<Database> openDb() async {
    _db ??= await openDatabase(
        join(await getDatabasesPath(), 'list3.db'),
        onCreate: (database, version) {
          database.execute("CREATE TABLE user (id INTEGER PRIMARY KEY, lastname TEXT, firstname TEXT, login TEXT, password TEXT, mail TEXT)");
          database.execute("CREATE TABLE student_group (id INTEGER PRIMARY KEY, label TEXT, order_by INTEGER)");
          database.execute("CREATE TABLE student (id INTEGER PRIMARY KEY, lastname TEXT, firstname TEXT, age INTEGER, id_student_group INTEGER, FOREIGN KEY(id_student_group) REFERENCES student_group(id))");
        }, version: _version);
    return _db;
  }

  Future testDb() async {
    _db = await openDb();
    await _db.execute("INSERT INTO user VALUES(2, 'admin', 'admin', 'admin', 'admin', 'admin@admin.fr')");
    List users = await _db.rawQuery("SELECT * FROM user");
    await _db.execute("INSERT INTO student_group VALUES(3, 'GROUPE 3', 1)");
    List groups = await _db.rawQuery("SELECT * FROM student_group");
    print(users[0].toString());
    print(groups[0].toString());
  }

  Future <User> createUser(User user) async {
    _db = await openDb();

    User newUser = await _db.insert('user', user.toMap(), conflictAlgorithms: ConflictAlgorithm.replace);

    return newUser;
  }

  Future<User> getUser(String login, String password) async {
    var user = await _db.query('user', where: 'login=? and password=?', whereArgs: [login, password]);
        return User(
          user[0]['id'],
          user[0]['lastname'],
          user[0]['firstname'],
          user[0]['login'],
          user[0]['password'],
          user[0]['mail'],
        );
  }

  Future<int> createUpdateGroup(Group group) async {
    int id = await _db.insert('group', group.toMap(), conflictAlgorithms: ConflictAlgorithm.replace);
    return id;
  }
}
