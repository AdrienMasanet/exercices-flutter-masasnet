import 'package:flutter/material.dart';
import 'package:formulaire/util/db_helper.dart';
import 'package:formulaire/view/home.dart';
import 'package:formulaire/view/login_form.dart';

void main() {
  runApp(App());
}

class App extends StatefulWidget {
  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  DbHelper dbHelper = DbHelper();

  @override
  Widget build(BuildContext context) {
    dbHelper.testDb();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Formulaire',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: "/",
      routes: {
        "/": (context) => const LoginForm(),
        "/accueil": (context) => const Home(),
      },
    );
  }
}
