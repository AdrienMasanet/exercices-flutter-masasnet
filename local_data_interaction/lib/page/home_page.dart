import 'dart:convert';
import 'dart:io';

import 'package:english_words/english_words.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController KeyInputControl = TextEditingController();
  TextEditingController ValueInputControl = TextEditingController();

  late File jsonFile;
  late Directory dir;
  String fileName = "monJson.json";
  bool fileExists = false;
  late Map<String, String> fileContent;

  @override
  void initState() {
    super.initState();
    getApplicationDocumentsDirectory().then((Directory directory) {
      print(directory.toString());
      dir = directory;
      jsonFile = File("${dir.path} / ${fileName}");
      fileExists = jsonFile.existsSync();
      if (fileExists) {
        setState(() => fileContent = jsonDecode(jsonFile.readAsStringSync()));

      } else {
        createFile(content, dir, fileName);
      }
      setState(() => fileContent = jsonDecode(jsonFile.readAsStringSync()));
      print(fileContent);
    });
  }

  @override
  void dispose() {
    KeyInputControl.dispose();
    ValueInputControl.dispose();
    super.dispose();
  }

  /*
  Créer fichier JSON
   */
  void createFile(Map<String, String> content, Directory dir, String fileName) {
    if (kDebugMode) {
      print("Création du fichier");
    }
    File file = File("${dir.path}/${fileName}");
    file.createSync();
    fileExists = true;
    file.writeAsStringSync(jsonEncode(content));
  }

  void writeToFile(String key, String value) {
    Map<String, String> content = {key: value};
    if (fileExists) {
      Map<String, String> jsonFileContent = jsonDecode(
          jsonFile.readAsStringSync());
    }
  }

  @override
  Widget build(BuildContext context) {
    final wordPair = WordPair.random();

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Mot au hasard"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Contenu du fichier :",
                style: TextStyle(fontWeight: FontWeight.bold),),
              Text(fileContent.toString()),
              const Padding(padding: EdgeInsets.only(top: 10.0)),
              const Text("Ajouter au JSON",
                style: TextStyle(fontWeight: FontWeight.bold),),
              TextField(
                controller: KeyInputControl,
              ),
              TextField(
                controller: ValueInputControl,
              ),
              const Padding(padding: EdgeInsets.only(top: 20.0)),
              ElevatedButton(onPressed: () =>
                  writeToFile(KeyInputControl.text, ValueInputControl.text),
                  child: Text("Ajouter au JSON")),
            ],
          ),
        ),
      ),
    );
  }
}
