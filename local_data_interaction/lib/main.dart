import 'package:flutter/material.dart';
import 'package:local_data_interaction/page/home_page.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const HomePage(
        title: 'Écriture dans fichier JSON',
      ),
    );
  }
}
